/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.mapper;

import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.*;
import org.mapstruct.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StoerungsauskunftMapper {

    final Pattern STREET_HOUSENUMBER_PATTERN = Pattern.compile("(^\\D+)(\\d.*)");

    @Mappings({
            @Mapping(target = "description", source = "comment"),
            @Mapping(target = "failureBegin", source = "date", dateFormat = "dd.MM.yyyy HH:mm:ss"),
            @Mapping(target = "longitude", source = "lng"),
            @Mapping(target = "latitude", source = "lat"),
            @Mapping(target = "street", source = "street", qualifiedByName = "streetMapperToForeignFailureDataDto"),
            @Mapping(target = "housenumber", source = "houseNo"),
    })
    ForeignFailureDataDto toForeignFailureDataDto(StoerungsauskunftUserNotification srcEntity);


    @AfterMapping
    default void afterMappingProcess(StoerungsauskunftUserNotification srcEntity, @MappingTarget ForeignFailureDataDto targetEntity){
        if (targetEntity.getHousenumber() == null || targetEntity.getHousenumber().isEmpty()){
            targetEntity.setHousenumber(housenumberMapperToForeignFailureDataDto(srcEntity.getStreet()));
        }
        targetEntity.setRadiusInMeters(0L);
        targetEntity.setVoltageLevel(Constants.VOLTAGE_LVL_LOW);
    }

    @Named("streetMapperToForeignFailureDataDto")
    default String streetMapperToForeignFailureDataDto(String streetFromUserNotification) {
        if (streetFromUserNotification == null || streetFromUserNotification.isEmpty()) return "";
        String street = streetFromUserNotification;
        Matcher matcher = STREET_HOUSENUMBER_PATTERN.matcher(streetFromUserNotification);
        if (matcher.matches()) {
            street = matcher.group(1).trim();
        }
        return street;
    }

    @Named("housenumberMapperToForeignFailureDataDto")
    default String housenumberMapperToForeignFailureDataDto(String streetFromUserNotification) {
        if (streetFromUserNotification == null || streetFromUserNotification.isEmpty()) return "";
        String housenumber = "";
        Matcher matcher = STREET_HOUSENUMBER_PATTERN.matcher(streetFromUserNotification);
        if (matcher.matches()) {
            housenumber = matcher.group(2).trim();
        }
        return housenumber;
    }

    /* Exportmapper */

    @AfterMapping
    default void setCoordinates(@MappingTarget StoerungsauskunftOutage target, FailureInformationDto srcEntity) {

        //Störungsauskunft.de Koordinatenformat: LNG,LAT;LNG,LAT; …

        List<ArrayList<BigDecimal>> addressPolygonPoints = srcEntity.getAddressPolygonPoints();
        if (addressPolygonPoints != null && !addressPolygonPoints.isEmpty()) {
            String coordinatesResult = "";
            for (ArrayList<BigDecimal> addressPolygonPoint : addressPolygonPoints) {
                //index 0: = Lat / index 1:= Lng
                coordinatesResult = coordinatesResult.concat(addressPolygonPoint.get(1).toString() + "," + addressPolygonPoint.get(0).toString() +";");
            }
            target.setCoordinates(coordinatesResult);
        } else {
            target.setCoordinates(srcEntity.getLongitude()+ "," + srcEntity.getLatitude());
        }

    }

    @Mappings({
            @Mapping(target = "liveInfo", source = "description"),
            @Mapping(target = "date", source = "failureBegin", dateFormat = "dd.MM.yyyy HH:mm:ss"),
            @Mapping(target = "estimatedEnding", source = "failureEndPlanned", dateFormat = "dd.MM.yyyy HH:mm:ss"),
    })
    StoerungsauskunftOutage toStoerungsauskunftOutage(FailureInformationDto srcEntity);
}
