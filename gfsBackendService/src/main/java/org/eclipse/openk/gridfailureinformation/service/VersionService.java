/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.mapper.VersionMapper;
import org.eclipse.openk.gridfailureinformation.model.Version;
import org.eclipse.openk.gridfailureinformation.repository.VersionRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.VersionDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VersionService {
    @Value("${backend-version}")
    public String backendVersion;

    private final VersionRepository versionRepository;

    private final VersionMapper versionMapper;

    public VersionService(VersionRepository versionRepository, VersionMapper versionMapper) {
        this.versionRepository = versionRepository;
        this.versionMapper = versionMapper;
    }

    public VersionDto getVersion() {
        VersionDto retVersion;
        Optional<Version> dbVersion = versionRepository.findById(1L);
        if (dbVersion.isPresent()) {
            retVersion = versionMapper.toVersionDto(dbVersion.get());
        }
        else {
            retVersion = new VersionDto();
            retVersion.setDbVersion(Constants.DB_VERSION_NOT_PRESENT);
        }
        retVersion.setBackendVersion(backendVersion);
        return retVersion;
    }
}
