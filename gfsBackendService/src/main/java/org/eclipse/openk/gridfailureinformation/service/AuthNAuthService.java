/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.api.AuthNAuthApi;
import org.eclipse.openk.gridfailureinformation.model.JwtToken;
import org.eclipse.openk.gridfailureinformation.model.LoginCredentials;
import org.eclipse.openk.gridfailureinformation.viewmodel.UserModulePortalDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Log4j2
@Service
public class AuthNAuthService {
    @Lazy
    private final AuthNAuthApi authNAuthApi;

    @Value(("${portalFeLoginURL}"))
    private String portalFeLoginUrl;

    @Value(("${feLoginURL}"))
    private String feLoginUrl;

    @Value(("${portalFeModulename}"))
    private String portalFeModulename;

    @Value("${services.authNAuth.technical-username}")
    private String technicalUsername;

    @Value("${services.authNAuth.technical-userpassword}")
    private String technicalUserPassword;

    public AuthNAuthService(@NonNull AuthNAuthApi authNAuthApi) {
        this.authNAuthApi = authNAuthApi;
    }

    public List<UserModulePortalDto> getUserModulePortalList() {
        JwtToken jwtToken = getTechnicalJWT();
        List<UserModulePortalDto> usemodulePortalLists = authNAuthApi.userModulesForUser(jwtToken.getAccessToken());
        this.logout(jwtToken.getAccessToken());
        return usemodulePortalLists;
    }

    public JwtToken getTechnicalJWT() {
        LoginCredentials loginCredentials = new LoginCredentials(technicalUsername, technicalUserPassword);
        return authNAuthApi.login(loginCredentials);
    }

    public void logout() {
        String bearerToken = (String) SecurityContextHolder.getContext().getAuthentication().getDetails();
        this.logout(bearerToken);
    }

    public void logout(String bearerToken) {
        authNAuthApi.logout(bearerToken).close();
    }

    public String getDirectMeasureLink(String gfiUUID) {
        // Example Link old: http://localhost:4220/#/gridMeasureDetail/5/edit?fwdUrl=http%3A%2F%2Flocalhost%3A4201%2F%23%2Flogin%3FfwdUrl%3Dhttp%3A%2F%2Flocalhost%3A4220%2F%26fwdId%3D5
        // new: http://entdockergss:4223/grid-failures/6432a9c9-0384-44af-9bb8-34f2878d7b49?fwdUrl=http%3A%2F%2Flocalhost%3A4201%2F%23%2Flogin%3FfwdUrl%3Dhttp%3A%2F%2Flocalhost%3A4220%2F%26fwdId%3D5

        String portalFeUrl = getPortalFeUrl();
        String stdGridmeasureURL = portalFeUrl + "/grid-failures/" + gfiUUID;
        String fwdURL = "fwdUrl=" + portalFeUrl + "&fwdId=" + gfiUUID;
        String fwdURLEncoded;
        String finalFwdUrl = portalFeLoginUrl + "?" + fwdURL;
        fwdURLEncoded = URLEncoder.encode(finalFwdUrl, StandardCharsets.UTF_8);

        return stdGridmeasureURL + "?fwdUrl=" + fwdURLEncoded;
    }

    public String getPortalFeUrl() {
        if (!feLoginUrl.isEmpty()) {
            return feLoginUrl;
        }

        List<UserModulePortalDto> userModulePortalList = getUserModulePortalList();
        return extractPortalFE(userModulePortalList);
    }

    private String extractPortalFE(List<UserModulePortalDto> userModulePortalList ) {
        String portalFeURL = "<PortalFE-URL>";
        for (UserModulePortalDto userModulePortal : userModulePortalList) {
            if (userModulePortal.getName().equalsIgnoreCase(portalFeModulename)){
                portalFeURL = userModulePortal.getLink();
                break;
            }
        }

        return portalFeURL;
    }
}
