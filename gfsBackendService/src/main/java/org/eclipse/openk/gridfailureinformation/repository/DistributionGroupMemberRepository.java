/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DistributionGroupMemberRepository extends JpaRepository<TblDistributionGroupMember, Long > {

    List<TblDistributionGroupMember> findAll();

    Optional<TblDistributionGroupMember> findByUuid(UUID uuid);

    @Query("select a from TblDistributionGroupMember a where a.tblDistributionGroup.uuid = :groupUuid")
    List<TblDistributionGroupMember> findByTblDistributionGroupUuid(@Param("groupUuid") UUID groupUuid);

    @Query("select count(*) from TblDistributionGroupMember a where a.tblDistributionGroup.id=:groupId and a.contactId=:contactId and uuid <>:memberUuid")
    Long countByDistributionGroupIdAndContactIdAndIsNotSame( @Param("groupId") Long groupId, @Param("contactId") UUID contactId, @Param("memberUuid") UUID memberUuid);

    @Query("select count(*) from TblDistributionGroupMember a where a.tblDistributionGroup.id=:groupId and a.contactId=:contactId")
    Long countByDistributionGroupIdAndContactId( @Param("groupId") Long groupId, @Param("contactId") UUID contactId);

}
