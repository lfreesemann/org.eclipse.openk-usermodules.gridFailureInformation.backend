/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;

@Log4j2
public class DecideFailureInfoCanceled extends DecisionTask<GfiProcessSubject> {

    public DecideFailureInfoCanceled() {
        super("Decision: Ist die Störungsinformation storniert?");
    }

    @Override
    public OutputPort decide(GfiProcessSubject subject) throws ProcessException {
        ProcessState newState = subject.getProcessHelper().getProcessStateFromStatusUuid(
                subject.getFailureInformationDto().getStatusInternId());

        String loggerOutput1 = "Decide: ";

        if (newState == GfiProcessState.CANCELED) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing YES");
            return OutputPort.YES;
        } else if (newState == GfiProcessState.QUALIFIED) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing NO");
            return OutputPort.NO;
        } else if (newState == GfiProcessState.COMPLETED) {
            log.debug(loggerOutput1 + getDescription() + "\" -> Firing Completed");
            return OutputPort.PORT3;
        } else {
            throw new ProcessException(this.getDescription() + ": Invalid status request:" + newState,
                    new InternalServerErrorException("invalid.status.request"));
        }
    }

}
