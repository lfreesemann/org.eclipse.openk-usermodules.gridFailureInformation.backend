/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.mapper.RadiusMapper;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.RadiusDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RadiusService {
    private final RadiusRepository radiusRepository;

    private final RadiusMapper radiusMapper;

    public RadiusService(RadiusRepository radiusRepository, RadiusMapper radiusMapper) {
        this.radiusRepository = radiusRepository;
        this.radiusMapper = radiusMapper;
    }

    public List<RadiusDto> getRadii() {
        return radiusRepository.findAll().stream()
                .map( radiusMapper::toRadiusDto )
                .collect(Collectors.toList());
    }

    public RadiusDto findByRadius(int radius ) {
        return radiusMapper.toRadiusDto(
                radiusRepository.findByRadius((long) radius)
                        .orElse( null ));
    }
}
