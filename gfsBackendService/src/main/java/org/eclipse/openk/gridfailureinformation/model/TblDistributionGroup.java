/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

import java.util.UUID;

@Data
@Entity
public class TblDistributionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_distribution_group_id_seq")
    @SequenceGenerator(name = "tbl_distribution_group_id_seq", sequenceName = "tbl_distribution_group_id_seq", allocationSize = 1)

    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private String name;

    private String emailSubjectPublish;
    private String distributionTextPublish;

    private String emailSubjectUpdate;
    private String distributionTextUpdate;

    private String emailSubjectComplete;
    private String distributionTextComplete;

    //Short (SMS)
    private String emailSubjectPublishShort;
    private String distributionTextPublishShort;

    private String emailSubjectUpdateShort;
    private String distributionTextUpdateShort;

    private String emailSubjectCompleteShort;
    private String distributionTextCompleteShort;


}
