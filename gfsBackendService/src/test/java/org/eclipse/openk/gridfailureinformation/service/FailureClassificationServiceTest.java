/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureClassificationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class FailureClassificationServiceTest {
    @Autowired
    private FailureClassificationService failureClassificationService;

    @Autowired
    private FailureClassificationRepository failureClassificationRepository;

    @Test
    public void shouldGetFailureClassificationsProperly() {
        List<RefFailureClassification> mockRefFailureClassificationList = MockDataHelper.mockRefFailureClassificationList();
        when(failureClassificationRepository.findAll()).thenReturn(mockRefFailureClassificationList);
        List<FailureClassificationDto> listRefFailureClassification = failureClassificationService.getFailureClassifications();

        assertEquals(listRefFailureClassification.size(), mockRefFailureClassificationList.size());
        assertEquals(listRefFailureClassification.size(), 2);
        assertEquals(listRefFailureClassification.get(1).getUuid(), mockRefFailureClassificationList.get(1).getUuid());
    }
}


