/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.support;

import org.eclipse.openk.gridfailureinformation.mailexport.dtos.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.mailexport.dtos.MailMessageDto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MockDataHelper {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private MockDataHelper() {}

    public static MailMessageDto mockMailMessageDto() {
        MailMessageDto mailMessageDto = new MailMessageDto();
        FailureInformationDto failureInformationDto = new FailureInformationDto();
        mailMessageDto.setFailureInformationDto(failureInformationDto);
        mailMessageDto.setBody("Subject: BetreffzeileTest Body: EmailText Content Test");
        mailMessageDto.setDistributionGroup("MailDistributionGroupTest");
        mailMessageDto.setEmailSubject("Test Betreff");

        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add("tester@tester.net");
        mailMessageDto.setMailAddresses(mailAddressList);

        return mailMessageDto;
    }

    public static MailMessageDto mockMailMessageDtoWrongRecipientFormat() {
        MailMessageDto mailMessageDto = new MailMessageDto();
        FailureInformationDto failureInformationDto = new FailureInformationDto();
        mailMessageDto.setFailureInformationDto(failureInformationDto);
        mailMessageDto.setBody("Subject: BetreffzeileTest Body: EmailText Content Test");
        mailMessageDto.setDistributionGroup("MailDistributionGroupTest");

        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add("testertester.net");
        mailMessageDto.setMailAddresses(mailAddressList);

        return mailMessageDto;
    }
}
